﻿/*
    Copyright 2013 abhijeet bhagat
 
    ZEdnet is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ZEdnet is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZEdnet.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using ZEdnet.CPU;
using WORD = System.UInt16;

namespace ZEdnet
{
    class Program
    {
        const int MAINMEMORYSIZE = 64 * 1024;
        static byte[] RAM = new byte[MAINMEMORYSIZE];

        static void Main(string[] args)
        {
            Z80CPU cpu = new Z80CPU();
            try
            {
                //Load RAM with boot file
                //loadRAM(args[0]);

                int j = 0;
                

                RAM[0] = 0x3e;
                RAM[1] = 0x11;

                RAM[j++] = RAM[j++];

                cpu.AF = 0xffff;

                for (int i = 0; i < 2; i++)
                {
                  switch (RAM[cpu.PC++])
                {
                    case 0x00: //NOP
                        break;
                    case 0x01: //LD BC, nn
                        cpu.BC = FetchWORD(cpu.PC);
                        cpu.PC += 2;
                        break;
                    case 0x02: //LD (BC), A
                        RAM[cpu.BC] = (byte)(cpu.AF >> 8);
                        printMemoryContents(RAM[cpu.BC]);
                        break;
                    case 0x06: //LD B, n
                        cpu.BC = (WORD)(cpu.BC & 0x00ff | (RAM[cpu.PC++] << 8));
                        break;
                    case 0x0e: //LD C, n
                        cpu.BC = (WORD)(cpu.BC & 0xff00 | RAM[cpu.PC++]);
                        break;
                    case 0x0a: //LD A, (BC)                        
                        cpu.AF = (WORD)((cpu.AF & 0x00ff) & RAM[cpu.BC]);
                        break;
                    case 0x11: //LD DE, nn
                        cpu.DE = FetchWORD(cpu.PC);
                        cpu.PC += 2;
                        break;
                    case 0x12: //LD (DE), A
                        RAM[cpu.DE] = (byte)(cpu.AF & 0xff00);
                        break;
                    case 0x1a: //LD A, (DE)                        
                        cpu.AF = (WORD)((cpu.AF & 0x00ff) & RAM[cpu.DE]);
                        break;
                    case 0x16: //LD D, n
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | (RAM[cpu.PC++] << 8));
                        break;
                    case 0x1e: //LD E, n
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | RAM[cpu.PC++]);
                        break;
                    case 0x2e: //LD L, n
                        cpu.HL = (WORD)(cpu.HL & 0xff00 | RAM[cpu.PC++]);
                        break;
                    case 0x26: //LD H, n
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | (RAM[cpu.PC++] << 8));
                        break;
                    case 0x32: //LD (nn), A
                        RAM[FetchWORD(cpu.PC)] = (byte)(cpu.AF & 0xff00);
                        break;
                    case 0x36: //LD (HL), n
                        RAM[cpu.HL] = RAM[cpu.PC++];
                        break;
                    case 0x3a: //LD A, (nn)
                        cpu.AF = FetchWORD(cpu.PC);
                        cpu.PC += 2;
                        break;
                    case 0x3d:

                        break;
                    case 0x3e: //LD A, n
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | (RAM[cpu.PC++] << 8));
                        break;

                    #region Load r, r'
                    case 0x40: //LD B, B
                        break;
                    case 0x41: //LD B, C
                        cpu.BC = (WORD)(cpu.BC & 0x00ff | ((cpu.BC & 0x00ff) << 8));
                        break;
                    case 0x42: //LD B, D
                        cpu.BC = (WORD)((cpu.BC & 0x00ff) | (cpu.DE & 0xff00));
                        break;
                    case 0x43: //LD B, E
                        cpu.BC = (WORD)((cpu.BC & 0x00ff) | ((cpu.DE & 0x00ff) << 8));
                        break;
                    case 0x44: //LD B, H
                        cpu.BC = (WORD)((cpu.BC & 0x00ff) | (cpu.HL & 0xff00));
                        break;
                    case 0x45: //LD B, L
                        cpu.BC = (WORD)((cpu.BC & 0x00ff) | ((cpu.HL & 0x00ff) << 8));
                        break;
                    case 0x46: //LD B, (HL)
                        cpu.BC = (WORD)((cpu.BC & 0x00ff) | (RAM[cpu.HL] << 8));
                        break;
                    case 0x47: //LD B, A
                        cpu.BC = (WORD)((cpu.BC & 0x00ff) | (cpu.AF & 0xff00));
                        break;
                    case 0x48: //LD C, B
                        cpu.BC = (WORD)((cpu.BC & 0xff00) >> 8 | (cpu.BC & 0xff00));
                        break;
                    case 0x49: //LD C, C
                        break;
                    case 0x4a: //LD C, D
                        cpu.BC = (WORD)((cpu.BC & 0xff00) | ((cpu.DE & 0xff00) >> 8));
                        break;
                    case 0x4b: //LD C, E
                        cpu.BC = (WORD)((cpu.BC & 0xff00) | (cpu.DE & 0x00ff));
                        break;
                    case 0x4c: //LD C, H
                        cpu.BC = (WORD)((cpu.BC & 0xff00) | ((cpu.HL & 0xff00) >> 8));
                        break;
                    case 0x4d: //LD C, L
                        cpu.BC = (WORD)((cpu.BC & 0xff00) | (cpu.HL & 0x00ff));
                        break;
                    case 0x4e: //LD C, (HL)
                        cpu.BC = (WORD)((cpu.BC & 0xff00) | RAM[cpu.HL]);
                        break;
                    case 0x4f: //LD C, A
                        cpu.BC = (WORD)((cpu.BC & 0xff00) | ((cpu.AF & 0xff00) >> 8));
                        break;
                    case 0x50: //LD D, B
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | (cpu.BC & 0xff00));
                        break;
                    case 0x51: //LD D, C
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | ((cpu.BC & 0x00ff) << 8));
                        break;
                    case 0x52: //LD D, D
                        break;
                    case 0x53: //LD D, E
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | ((cpu.DE & 0x00ff) << 8));
                        break;
                    case 0x54: //LD D, H
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | (cpu.HL & 0xff00));
                        break;
                    case 0x55: //LD D, L
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | (cpu.HL & 0x00ff) << 8);
                        break;
                    case 0x56: //LD D, (HL)
                        cpu.DE = (WORD)((cpu.DE & 0x00ff) | (RAM[cpu.HL] << 8));
                        break;
                    case 0x57: //LD D, A
                        cpu.DE = (WORD)(cpu.DE & 0x00ff | (cpu.AF & 0xff00));
                        break;
                    case 0x58: //LD E, B
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | ((cpu.BC & 0xff00) >> 8));
                        break;
                    case 0x59: //LD E, C
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | (cpu.BC & 0x00ff));
                        break;
                    case 0x5a: //LD E, D
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | ((cpu.DE & 0xff00) >> 8));
                        break;
                    case 0x5b: //LD E, E
                        break;
                    case 0x5c: //LD E, H
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | ((cpu.HL & 0xff00) >> 8));
                        break;
                    case 0x5d: //LD E, L
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | (cpu.HL & 0x00ff));
                        break;
                    case 0x5e: //LD E, (HL)
                        cpu.DE = (WORD)((cpu.DE & 0xff00) | RAM[cpu.HL]);
                        break;
                    case 0x5f: //LD E, A
                        cpu.DE = (WORD)(cpu.DE & 0xff00 | ((cpu.AF & 0xff00) >> 8));
                        break;
                    case 0x60: //LD H, B
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | (cpu.BC & 0xff00));
                        break;
                    case 0x61: //LD H, C
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | ((cpu.BC & 0x00ff) << 8));
                        break;
                    case 0x62: //LD H, D
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | (cpu.DE & 0xff00));
                        break;
                    case 0x63: //LD H, E
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | ((cpu.DE & 0x00ff) << 8));
                        break;
                    case 0x64: //LD H, H
                        break;
                    case 0x65: //LD H, L
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | ((cpu.HL & 0x00ff) << 8));
                        break;
                    case 0x66: //LD H, (HL)
                        cpu.HL = (WORD)((cpu.HL & 0x00ff) | (RAM[cpu.HL] << 8));
                        break;
                    case 0x67: //LD H, A
                        cpu.HL = (WORD)(cpu.HL & 0x00ff | (cpu.AF & 0xff00));
                        break;
                    case 0x68: //LD L, B
                        cpu.HL = (WORD)(cpu.HL & 0xff00 | ((cpu.BC & 0xff00) >> 8));
                        break;
                    case 0x69: //LD L, C
                        cpu.HL = (WORD)(cpu.DE & 0xff00 | (cpu.BC & 0x00ff));
                        break;
                    case 0x6a: //LD L, D
                        cpu.HL = (WORD)(cpu.HL & 0xff00 | ((cpu.DE & 0xff00) >> 8));
                        break;
                    case 0x6b: //LD L, E
                        cpu.HL = (WORD)(cpu.HL & 0xff00 | (cpu.DE & 0x00ff));
                        break;
                    case 0x6c: //LD L, H
                        cpu.HL = (WORD)(cpu.HL & 0xff00 | ((cpu.HL & 0xff00) >> 8));
                        break;
                    case 0x6d: //LD L, L
                        break;
                    case 0x6e: //LD L, (HL)
                        cpu.HL = (WORD)((cpu.HL & 0xff00) | RAM[cpu.HL]);
                        break;
                    case 0x6f: //LD L, A
                        cpu.HL = (WORD)(cpu.HL & 0xff00 | ((cpu.AF & 0xff00) >> 8));
                        break;
                    case 0x70: //LD (HL), B
                        RAM[cpu.HL] = (byte)(cpu.BC & 0xff00);
                        break;
                    case 0x71: //LD (HL), C
                        RAM[cpu.HL] = (byte)(cpu.BC & 0x00ff);
                        break;
                    case 0x72: //LD (HL), D
                        RAM[cpu.DE] = (byte)(cpu.DE & 0xff00);
                        break;
                    case 0x73: //LD (HL), E
                        RAM[cpu.DE] = (byte)(cpu.DE & 0x00ff);
                        break;
                    case 0x74: //LD (HL), H
                        RAM[cpu.HL] = (byte)(cpu.HL & 0xff00);
                        break;
                    case 0x75: //LD (HL), L
                        RAM[cpu.HL] = (byte)(cpu.HL & 0x00ff);
                        break;
                    case 0x77: //LD (HL), A
                        RAM[cpu.AF] = (byte)(cpu.AF & 0xff00);
                        break;
                    case 0x78: //LD A, B
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | (cpu.BC & 0xff00));
                        break;
                    case 0x79: //LD A, C
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | ((cpu.BC & 0x00ff) << 8));
                        break;
                    case 0x7a: //LD A, D
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | (cpu.DE & 0xff00));
                        break;
                    case 0x7b: //LD A, E
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | ((cpu.DE & 0x00ff) << 8));
                        break;
                    case 0x7c: //LD A, H
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | (cpu.HL & 0xff00));
                        break;
                    case 0x7d: //LD A, L
                        cpu.AF = (WORD)(cpu.AF & 0x00ff | ((cpu.HL & 0x00ff) << 8));
                        break;
                    case 0x7f: //LD A, A
                        break; 
                    case 0x7e: //LD A, (HL)
                        cpu.AF = (WORD)((cpu.AF & 0x00ff) | (RAM[cpu.HL] << 8));
                        break;
                    #endregion
                    case 0xC3: //JP nn
                        cpu.PC = FetchWORD(cpu.PC);
                        break;
                    case 0xdd: //LD r, (IX+d)
                        switch (RAM[cpu.PC++])
	                    {
                            case 0x36:
                                RAM[cpu.IX + RAM[cpu.PC++]] = RAM[cpu.PC++];
                                break;
                            case 0x46: //LD B, (IX+d)
                                cpu.BC = (WORD)((cpu.BC & 0x00ff) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
                            case 0x4e: //LD C, (IX+d)
                                cpu.BC = (WORD)((cpu.BC & 0xff00) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
                            case 0x56: //LD D, (IX+d)
                                cpu.DE = (WORD)((cpu.DE & 0x00ff) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
                            case 0x5e: //LD E, (IX+d)
                                cpu.DE = (WORD)((cpu.DE & 0xff00) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
                            case 0x66: //LD H, (IX+d)
                                cpu.HL = (WORD)((cpu.HL & 0x00ff) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
                            case 0x6e: //LD L, (IX+d)
                                cpu.HL = (WORD)((cpu.HL & 0xff00) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
                            case 0x70: //LD (IX+d), B
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.BC & 0xff00);
                                break;
                            case 0x71: //LD (IX+d), C
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.BC & 0x00ff);
                                break;
                            case 0x72: //LD (IX+d), D
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.DE & 0xff00);
                                break;
                            case 0x73: //LD (IX+d), E
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.DE & 0x00ff);
                                break;
                            case 0x74: //LD (IX+d), H
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.HL & 0xff00);
                                break;
                            case 0x75: //LD (IX+d), L
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.HL & 0x00ff);
                                break;
                            case 0x77: //LD (IX+d), A
                                RAM[cpu.IX + RAM[cpu.PC++]] = (byte)(cpu.AF & 0xff00);
                                break;
                            case 0x7e: //LD A, (IX+d)
                                cpu.AF = (WORD)((cpu.AF & 0x00ff) | RAM[cpu.IX + RAM[cpu.PC]]);
                                break;
		                    default:
                                throw new InvalidDataException(string.Format("Invalid instruction at {0}", cpu.PC));
	                    }
                        break;
                      case 0xed:
                        switch (RAM[cpu.PC++])
	                    {
                            case 0x57: //LD A, I
                                cpu.AF = (WORD)((cpu.AF & 0x00ff) | cpu.I);
                                //TODO set condition bits
                                break;
		                    default:
                                break;
	                    }
                        break;
                      case 0xfd: //LD r, (IY+d)
                        switch (RAM[cpu.PC++])
                        {
                            case 0x36:
                                RAM[cpu.IY + RAM[cpu.PC++]] = RAM[cpu.PC++];
                                break;
                            case 0x46: //LD B, (IY+d)
                                cpu.BC = (WORD)((cpu.BC & 0x00ff) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            case 0x4e: //LD C, (IY+d)
                                cpu.BC = (WORD)((cpu.BC & 0xff00) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            case 0x56: //LD D, (IY+d)
                                cpu.DE = (WORD)((cpu.DE & 0x00ff) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            case 0x5e: //LD E, (IY+d)
                                cpu.DE = (WORD)((cpu.DE & 0xff00) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            case 0x66: //LD H, (IY+d)
                                cpu.HL = (WORD)((cpu.HL & 0x00ff) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            case 0x6e: //LD L, (IY+d)
                                cpu.HL = (WORD)((cpu.HL & 0xff00) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            case 0x70: //LD (IY+d), B
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.BC & 0xff00);
                                break;
                            case 0x71: //LD (IY+d), C
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.BC & 0x00ff);
                                break;
                            case 0x72: //LD (IY+d), D
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.DE & 0xff00);
                                break;
                            case 0x73: //LD (IY+d), E
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.DE & 0x00ff);
                                break;
                            case 0x74: //LD (IY+d), H
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.HL & 0xff00);
                                break;
                            case 0x75: //LD (IY+d), L
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.HL & 0x00ff);
                                break;
                            case 0x77: //LD (IY+d), A
                                RAM[cpu.IY + RAM[cpu.PC++]] = (byte)(cpu.AF & 0xff00);
                                break;
                            case 0x7e: //LD A, (IY+d)
                                cpu.AF = (WORD)((cpu.AF & 0x00ff) | RAM[cpu.IY + RAM[cpu.PC]]);
                                break;
                            default:
                                throw new InvalidDataException(string.Format("Invalid instruction at {0}", cpu.PC));
                        }
                        break;
                    default:
                        throw new InvalidDataException(string.Format("Invalid instruction at {0}", cpu.PC));
                }  
                }
                

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine(cpu);
            Console.ReadKey();
        }

        private static void printMemoryContents(byte content)
        {
            Console.WriteLine("0x{0:x}", content);
        }

        private static WORD FetchWORD(WORD PC)
        {
            return (WORD)(RAM[PC + 1] << 8 | RAM[PC]);
        }

        private static void loadRAM(string bootPath)
        {
            try
            {
                RAM = File.ReadAllBytes(bootPath);
            }
            catch (FileNotFoundException)
            {
                throw;
            }
        }
    }
}
