﻿/*
    Copyright 2013 abhijeet bhagat
 
    ZEdnet is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ZEdnet is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ZEdnet.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using WORD = System.UInt16;

namespace ZEdnet.CPU
{
    class Z80CPU
    {
        public WORD AF { get; set; }
        public WORD BC { get; set; }
        public WORD DE { get; set; }
        public WORD HL { get; set; }

        public WORD AF_ { get; set; }
        public WORD BC_ { get; set; }
        public WORD DE_ { get; set; }
        public WORD HL_ { get; set; }

        public WORD IX { get; set; }
        public WORD IY { get; set; }
        public WORD SP { get; set; }

        public byte I { get; set; }
        public byte R { get; set; }

        public WORD PC { get; set; }

        byte Flags { get; set; }

        public override string  ToString()
        {
 	         return String.Format("AF = 0x{0:x}\n" +
                                  "BC = 0x{1:x}\n" +
                                  "DE = 0x{2:x}\n" +
                                  "HL = 0x{3:x}\n" + 
                                  "PC = 0x{4:x}",
                                   AF,
                                   BC,
                                   DE,
                                   HL,
                                   PC
                                  );
        }
    }
}
